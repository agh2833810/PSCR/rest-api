cmake_minimum_required(VERSION 3.22)
project(rest-api)

set(CMAKE_CXX_STANDARD 20)

find_package(CURL REQUIRED)

include_directories(${CURL_INCLUDE_DIR} Inc nlohmann)

set(SOURCE_FILES
        web-scraping.cpp
        Src/send_functions.cpp)

add_executable(rest-api ${SOURCE_FILES})

target_link_libraries(rest-api ${CURL_LIBRARIES})
