#include "Inc/send_functions.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <curl/curl.h>
#include <mutex>
#include <thread>
#include <stdlib.h>
#include <strings.h>
#include <string.h> 
#include <json.hpp>
#include <chrono>
#include <thread>
#include <sys/socket.h>
#include <arpa/inet.h>

using json = nlohmann::json;

#define OK_STATUS 0
#define NOT_OK_STATUS 1
#define PLACES_AMOUNT 174

struct JSONData {
    double temperature_min;
    double temperature_max;
    double wind_speed;
    double clouds;
};

struct WeatherData {
    double temperature[PLACES_AMOUNT];
    double wind[PLACES_AMOUNT];
    double insolation[PLACES_AMOUNT];
};

JSONData json_data;
WeatherData weather_data;

size_t data_to_buff(void *ptr, size_t size, size_t nmemb, void *userdata);
int web_scraping(const std::string &latitude, const std::string &longitude, std::string &response_stream);
void send_data_server(const WeatherData &data, int sock) ;

int main() 
{
    int Ret_Status;
    const std::string places[] = {"55", "18", "54.5", "16.5", "54.5", "17", "54.5", "17.5", "54.5", "18", "54.5", "18.5", "54.5", "19", "54.5", "19.5", "54", "14.5", "54", "15", 
        "54", "15.5", "54", "16", "54", "16.5", "54", "17", "54", "17.5", "54", "18", "54", "18.5", "54", "19", "54", "19.5", "54", "20", "54", "20.5", 
        "54", "21", "54", "21.5", "54", "22", "54", "22.5", "54", "23", "54", "23.5", "53.5", "14.5", "53.5", "15", "53.5", "15.5", "53.5", "16", 
        "53.5", "16.5", "53.5", "17", "53.5", "17.5", "53.5", "18", "53.5", "18.5", "53.5", "19", "53.5", "19.5", "53.5", "20", "53.5", "20.5", 
        "53.5", "21", "53.5", "21.5", "53.5", "22", "53.5", "22.5", "53.5", "23", "53.5", "23.5", "53", "14.5", "53", "15", "53", "15.5", "53", "16", 
        "53", "16.5", "53", "17", "53", "17.5", "53", "18", "53", "18.5", "53", "19", "53", "19.5", "53", "20", "53", "20.5", "53", "21", "53", "21.5", 
        "53", "22", "53", "22.5", "53", "23", "53", "23.5", "52.5", "14.5", "52.5", "15", "52.5", "15.5", "52.5", "16", "52.5", "16.5", "52.5", "17", 
        "52.5", "17.5", "52.5", "18", "52.5", "18.5", "52.5", "19", "52.5", "19.5", "52.5", "20", "52.5", "20.5", "52.5", "21", "52.5", "21.5", 
        "52.5", "22", "52.5", "22.5", "52.5", "23", "52.5", "23.5", "52", "14.5", "52", "15", "52", "15.5", "52", "16", "52", "16.5", "52", "17", 
        "52", "17.5", "52", "18", "52", "18.5", "52", "19", "52", "19.5", "52", "20", "52", "20.5", "52", "21", "52", "21.5", "52", "22", "52", "22.5", 
        "52", "23", "51.5", "23.5", "51.5", "14.5", "51.5", "15", "51.5", "15.5", "51.5", "16", "51.5", "16.5", "51.5", "17", "51.5", "17.5", "51.5", 
        "18", "51.5", "18.5", "51.5", "19", "51.5", "19.5", "51.5", "20", "51.5", "20.5", "51.5", "21", "51.5", "21.5", "51.5", "22", "51.5", "22.5", 
        "51.5", "23", "51.5", "23.5", "51", "14.5", "51", "15", "51", "15.5", "51", "16", "51", "16.5", "51", "17", "51", "17.5", "51", "18", "51", 
        "18.5", "51", "19", "51", "19.5", "51", "20", "51", "20.5", "51", "21", "51", "21.5", "51", "22", "51", "22.5", "51", "23", "51", "23.5", 
        "50.5", "16.5", "50.5", "17", "50.5", "17.5", "50.5", "18", "50.5", "18.5", "50.5", "19", "50.5", "19.5", "50.5", "20", "50.5", "20.5", "50.5", 
        "21", "50.5", "21.5", "50.5", "22", "50.5", "22.5", "50.5", "23", "50", "18", "50", "18.5", "50", "19", "50", "19.5", "50", "20", "50", "20.5", 
        "50", "21", "50", "21.5", "50", "22", "50", "22.5", "50", "23", "49.5", "19", "49.5", "19.5", "49.5", "20", "49.5", "20.5", "49.5", "21", 
        "49.5", "21.5", "49.5", "22", "49.5", "22.5"
    };

    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    {
        std::cerr << "Socket creation error\n";
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(12345); // Port na którym nasłuchuje serwer
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    // Zastąp "127.0.0.1" adresem IP serwera
    // if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) 
    // {
    //     std::cerr << "Invalid address/ Address not supported\n";
    //     return;
    // }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    {
        std::cerr << "Connection Failed\n";
        return -1;
    }

    while(true) {
        std::ofstream raw_data_file("weather_data.json");
        if(!raw_data_file) 
        {
            std::cerr << "Nie można otworzyć pliku weather_data.json.\n";
            return NOT_OK_STATUS;
        }

        json all_data_json = json::array();

        for(int i = 0; i < 2 * PLACES_AMOUNT; i += 2) {
            std::string response_str;
            Ret_Status = web_scraping(places[i], places[i+1], response_str);

            if(Ret_Status == OK_STATUS) 
            {
                json response_json = json::parse(response_str);
                all_data_json.push_back(response_json);
            } 
            else 
            {
                std::cerr << "Błąd podczas pobierania danych dla lokalizacji: " << places[i] << ", " << places[i+1] << std::endl;
            }
        }

        raw_data_file << all_data_json.dump(4);
        raw_data_file.close();

        std::ofstream filtered_data_file("dane_poprawne.json");
        if(!filtered_data_file) {
            std::cerr << "Nie można otworzyć pliku dane_poprawne.json.\n";
            return NOT_OK_STATUS;
        }

        json filtered_data_json = json::array();
        int iter = 0;
        std::cout << "Getting data!!" << std::endl;
        for(const auto &entry : all_data_json) 
        {
            json filtered_entry = {
                {"wind_speed", entry["wind"]["speed"]},
                {"clouds", entry["clouds"]["all"]},
                {"temp_max", entry["main"]["temp_max"]},
                {"temp_min", entry["main"]["temp_min"]}
            };
            filtered_data_json.push_back(filtered_entry);

            // Przypisanie wartości do struktury JSONData
            json_data.wind_speed = entry["wind"]["speed"].get<double>();
            json_data.clouds = entry["clouds"]["all"].get<double>();
            json_data.temperature_max = entry["main"]["temp_max"].get<double>();
            json_data.temperature_min = entry["main"]["temp_min"].get<double>();

            // Obliczanie wartości WeatherData
            weather_data.temperature[iter] = (json_data.temperature_max + json_data.temperature_min) / 2;
            weather_data.insolation[iter] = 100 - json_data.clouds;
            weather_data.wind[iter] = json_data.wind_speed;

            // Wysyłanie danych do serwera
            iter++;
        }
        std::cout << "Data Recieved!!" << std::endl;
        send_data_server(weather_data, sock);

        filtered_data_file << filtered_data_json.dump(4);
        filtered_data_file.close();

        if(Ret_Status != OK_STATUS) 
        {
            return NOT_OK_STATUS;
        }

        // Czekaj 15 minut
        std::this_thread::sleep_for(std::chrono::minutes(15));
    }

    return OK_STATUS;
}

// Funkcja zapisująca pobrane dane do bufora
size_t data_to_buff(void *ptr, size_t size, size_t nmemb, void *userdata) 
{
    std::string *response_stream = static_cast<std::string *>(userdata);
    size_t totalSize = size * nmemb;
    response_stream->append(static_cast<char *>(ptr), totalSize);
    return totalSize;
}

int web_scraping(const std::string &latitude, const std::string &longitude, std::string &response_stream) {
    CURL *curl;
    CURLcode res;

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();

    if(curl) 
    {
        std::string url = "https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&units=metric&appid=fdd4d80d8846663b4c01a8b209c35b64";

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, data_to_buff);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_stream);

        res = curl_easy_perform(curl);
        if(res != CURLE_OK) 
        {
            std::cerr << "Błąd podczas pobierania danych: " << curl_easy_strerror(res) << std::endl;
            curl_easy_cleanup(curl);
            curl_global_cleanup();
            return NOT_OK_STATUS;
        }

        curl_easy_cleanup(curl);
    } 
    else 
    {
        std::cerr << "Wystąpił problem z biblioteką curl\n";
        curl_global_cleanup();
        return NOT_OK_STATUS;
    }

    curl_global_cleanup();
    return OK_STATUS;
}


void send_data_server(const WeatherData &data, int sock) 
{
    // Tworzenie JSONa z danymi do wysłania
    json data_json = {
        {"temperature", data.temperature},
        {"insolation", data.insolation},
        {"wind", data.wind}
    };
    std::ostringstream data_str_stream;
    data_str_stream << "{\n\"data\": {\n\"weather_data\": [\n";

    for(int i = 0; i < PLACES_AMOUNT; i++){
        data_str_stream << "{\n" <<"\"temperature\":" <<data.temperature[i]<< ",\n";
        data_str_stream << "\"insolation\":" << data.insolation[i]<< ",\n";
        data_str_stream <<"\"wind\":" << data.wind[i]<< ",\n},\n";
    }
    data_str_stream << "\n]\n}\n}";

    std::string data_str = data_str_stream.str();
    std::cout <<data_str<<std::endl;
    send(sock, data_str.c_str(), data_str.size(), 0);
}
