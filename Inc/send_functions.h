#ifndef WEB_SCRAPING_SEND_FUNCTIONS_H
#define WEB_SCRAPING_SEND_FUNCTIONS_H

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <exception>
#include <iostream>
#include <cstdint>

void send_string(const std::string &msg);

class SendException : public std::exception {
public:
    SendException(const std::string& msg, std::uint8_t id) : message_(msg), error_id_(id) {}

    const char* what() const noexcept override {return message_.c_str();}
    std::uint8_t get_error_id() const {return error_id_;}
private:
    std::uint8_t error_id_;
    std::string message_;
};

class SendMessages{
public:
    SendMessages(uint16_t port, const char *host): port_(port), host_(host), opened_(false) {};

    void reconnect();
    void establish_connection() {reconnect();}

    ~SendMessages();

    void send_message(std::string &msg);
private:
    std::uint16_t port_;
    const char *host_;
    int sockfd_;
    bool opened_;

    void close_();
};

#endif //WEB_SCRAPING_SEND_FUNCTIONS_H
